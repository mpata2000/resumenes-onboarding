# Criptografia


- Cifrado: Proceso de transformación de un mensaje para que sea incomprensible o, al menos, dificil de comprender sin la clave de descifrado.

## Cifrado

El mensaje a cifrar se representa como una cadena de bits que, al cifrarla, se transforma en otra cadena. 
Según como se procesa esta cadena los algoritmos se clasifican en:

- **Stream ciphers - Cifrado por flujo:** Se procesa la cadena bit a bit. Se podría decir que el tamaño es de 1 bit.

![](Images/CifradoFlujo.png)

- **Block ciphers - Cifrado por bloque:** Se procesa la cadena de bloques en n bits.

![](Images/CifradoBloque.png)

### Tamaño de bloque

  - Normalmente multiplo de 8 para facilitar los calculos en los procesadores.
  - La cantidad de combinaciones de bits para un bloque de mensaje es 2^n, por ende n no debería ser demasiado pequeño porque facilitaría un ataque por diccionario (construyendo un diccionario de pares plaintext/ciphertext)
  - No debería ser demasiado grande porque sería poco eficiente
  - La mayoría de las veces el tamaño del mensaje no es múltiplo entero del tamaño de bloque, con lo cual el último bloque normalmente tiene menos bits que el tamaño de bloque. 
    - Por ejemplo un mensaje de 130 bits cifrado en bloques de 64 bits generaría 2 bloques de 64 bits y uno de 2 bits. En esos casos se agrega relleno o padding hasta completar el bloque. En el ejemplo anterior, se agregarían 62 bits de padding

### **Tipos de Cifrado**

- Cifrado simétrico o de llave privada → Se utiliza la misma llave para cifrar y descifrar.
- Cifrado asimétrico o de llave pública → Se utiliza un par de llaves distintas para cifrar y descifrar.

### **Cifrado simetrico**

![](Images/CifradoSimetrico.png)

**Que se necesita para utlizar el cifrado simétrico?**

- Intercambio de llaves. Es necesario que el emisor y receptor acuerden una llave a utilizar antes de iniciar la comunicación. Para ello se debe utilizar algún mecanismo de intercambio de llaves.
- Confianza. Ambas partes deben confiar en la otra, ya que no es posible autenticar en este tipo de sistemas.

**Algoritmos de cifrado simétrico**

- DES (Data Encryption Standard)
- 3DES (triple DES) 
- AES (Advance Encryption Standard)

#### DES

- Tamaño de bloque 64 bits
- Tamaño de llave. 56 bits (tiene 64 bits pero 8 no son utilizados por el algoritmo de cifrado).
- Es considerado inseguro debido a la corta longitud de la llave. Se utiliza su variante 3DES

#### 3DES

- Tamaño de bloque 64 bits
- Tamaño de llave. 112 bits (2 x 56) o 168bits (3 x 56).
- Emplea el algoritmo DES 3 veces para aumentar el tamaño de la llave

##### Esquema 3DES

![](Images/3DES.png)

Proceso de cifrado

1. Cifrar el mensaje utilizando DES con llave K1
2. Luego descifrarlo utilizando DES con la llave K2
3. Finalmente cifrarlo utilizando DES con llave K3

Proceso de descifrado

1. Descifrar el mensaje utilizando DES con la llave K3
2. Luego cifrarlo utilizando DES con llave K2
3. Finalmente descifrar el mensaje utilizando DES con la llave K

### Cifrado Asimetrico

![](Images/CifradoAsimetrico.png)

**Como funciona?**

- Si utiliza una llave pública para descifrar, entonces se utilizará una llave privada para cifrar.
- Ambas llaves están relacionadas matemáticamente pero no es posible obtener la llave privada a partir de la pública.

**Algoritmos de cifrado asimétrico**

- RSA (Ron Rivest, Adi Shamir y Len Adleman).
- Diffie-Hellman (para intercambio de llaves).

#### RSA

- Su fortaleza radica en la dificultad de factorizar números primos.
- Consiste en dos partes: La generación del par de llaves y el cifrado/descifrado

![](Images/RSA.png)

## Manejo de llaves

En que consiste el manejo de llaves?

- Generación
- Intercambio
- Almacenamiento
- Uso
- Destrucción
- Actualización

Esquemas del manejo de llaves

- Predistribución de llaves.
- Distribución de llaves de sesión.
- Acuerdo de intercambio de llaves.

### Predistribución de llaves

Cada participante del sistema tiene las llaves pre-cargadas de manera segura previamente a su uso. En este sistema no se permite el intercambio posterior de llaves.

### **Distribución de llaves de sesión**

Se generan y distrubuyen las llaves validas que pertenecen a una sesión. Son actualizadas periódicamente. Normalmente requieren de una llave pre-instalada de forma segura que se utilizará para la distribución segura de las llaves de sesión.

#### **Master/session**

![](Images/Master-session.png)

Pasos:

1. El POS le pide al Adquirente el WK.
2. El adquirente le envia la WK al POS cifrada con la MK.
3. El POS decifra al WK y la almacena de forma segura.
4. Se envían las transaciones cifradas con la WK 

#### DUKPT

![](Images/DUKPT.png)

1. El POS genera una llave derivada de la Llave pre-instalada
2. POS envia el mensaje junto con info para que el adquirente sepa como generar la llave derivada.
3. Se destruye la llave derivada

## CVV CVC

Se generan en el adquirer, ellos tienen claves para generar esos numeros.

## PIN (personal identification number)

hay tarjetas que utilizan un pin de ingreso como clave de acceso a una transaccion,

por ejemplo las tarjetas master. (el pin es una clave unica que solo sabe el dueno del plastico).


el punto de venta tiene injectado una master key.

### Pin block:

Es la combinacion de la masterkey el numero de la tarjeta y el pin number encriptado por un algoritmo triple des. Todos estos datos van a viajar encriptados para una transaccion que requiera pin en el campo iso 52.

El host puede desencriptar con la masterkey.

#### Genreacion de PINBlock

Para generar un PINBlock en ISO-0 necesitamos:
- PIN
- Longitud de PIN
- Padding
- PAN

Despues se generan dos numeros a partir de eso ( + es concatenacion)

- Numero 1: 0 + Long + PIN + Padding hasta llegar a 16 bytes
- Numero 2:  0000 + PAN sin los primeros 3 numeros y sin el ultimo

Se aplica XOR entre el numero 1 y el numero 2 ( num1 xor num2 )

Por ultimo se lo encripta con 3DES

## Service code:

es el campo que esta en la banda magnetica define si una transaccion requiere pin o no. (a traves del 3er bit)