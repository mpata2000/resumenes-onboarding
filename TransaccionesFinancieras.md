"Cuando uno realiza una compra esa compra termina guardada en un archivo de lote, el archivo de lote es un archivo que se genera en el punto de venta con todas las transacciones realizadas en el día, al fin del día se realiza un cierre de ese lote para transmitir los balances, anulaciones y ventas que se realizaron en el día"

***Una transaccion financiera es un mensaje, una operacion en la que se intercambian bienes y servicios a traves de un pago*** 

Compra es una transacción financiera

Por cada transacción se genera un numero de campo iso 62 ticket(cupon), este numero de ticket comienzo con la numeracion en 999 y luego seguira por el 0.

## Anulación 

Es una transacción financiera que termina anulando a una compra en un momento determinado, una anulación solamente se puede hacer en el lote actual ( si el lote se cerro, ese lote se borra del punto de venta, quiere decir que ya fueron transmitidas al host).

## Devolución 

es una anulación que hace referencia a una compra que no existe en el lote, sino que la tengo que referenciar con una compra que obtuvo el host.

Advice es un mensaje 220 que se le envia al host cuando haces una transaccion offline, entonces el host guarda esa info

### Campos importantes

2- Tipo de mensaje (MTI)

11- numero de trace: es un numero de secuencia que me permite especificar el requerimiento (viaja tanto en la req como en la rta). Se produce en el punto de venta. Se utiliza para vincular req con res

37 - retrival reference number. Este numero de secuencia se produce en el host y se vincula con el numero de trace.

## Transacciones offline

Cuando no hay comunicación con el host las transacciones se guardan en el batch local y luego son despachadas justo después de que el sistema este online y hagamos la primer transacción 

## Mensaje reverso

El mensaje de reveso significa que le tengo que avisar al host que en una transacción previa genere un requerimiento pero nunca tuve una respuesta del host, en ese caso es cuando se genera un reverso.

El mensaje de reverso contiene los mismos datos que el original pero cambia el mti(tipo de mensaje) apartir del numero de trace del sistema. Este no incrementa el numero de trace.

Se envía junto con la primer transacción que se haga cuando el sistema este nuevamente online.

## Orden de despacho luego de estar offline

reverso → transacción online → transacciones offline

"Antes del cierre del lote se chequea por transacciones pendientes"

La forma que le digo al host que tengo una transacción de reverso y ese reverso esta relacionado con una original que el host mantiene lo voy a hacer a partir del numero de trace del sistema del reverso que es exactamente igual al de la original.

## Cierre de lote

Mensaje 0500

La info de la operación original se transmite en el campo iso 63 que es el campo de cierre. Va a tener:

- Numero de batch/lote
- Contador de compras
- Monto total de las compras
- Contador de devoluciones
- Monto total de devoluciones
- Contador de anulaciones
- Monto total de anulaciones

El cierre de lote es una operación de conciliación, ya que el host tiene todas las operaciones realizadas por el pos también. 

Si no concilia la info que tenia el pos sobre las operaciones y el host, el host manda por el campo iso 39 un código de respuesta avisando que no hay conciliación. En ese caso se pide un batch upload.

## Batch upload

Mensaje de conciliación por diferencias en el cierre de lote, el pos le envía todas las transacciones otra vez.