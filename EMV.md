# EMV

Es un metodo de pago creado por Europapay, Mastercard y Visa (EMV)

## TOC

- [EMV](#emv)
  - [TOC](#toc)
  - [¿Que es EMV?](#que-es-emv)
  - [¿Que es un TPV?](#que-es-un-tpv)
  - [AID Application Identifier](#aid-application-identifier)
    - [Seleccion de Aplicaciones](#seleccion-de-aplicaciones)
      - [Comparaciones de AID](#comparaciones-de-aid)
      - [Selección por PSE](#selección-por-pse)
      - [SELECCIÓN FINAL](#selección-final)
      - [Selección por lista de AID’s](#selección-por-lista-de-aids)
  - [¿Que es un Data Object?](#que-es-un-data-object)
    - [Data Object List](#data-object-list)
    - [TLV - Tag Length Value](#tlv---tag-length-value)
      - [Tag](#tag)
      - [Length](#length)
      - [Value](#value)
  - [Inicio de transaccion](#inicio-de-transaccion)
  - [Verficacion](#verficacion)
    - [TSI](#tsi)
    - [TVR](#tvr)
    - [ODA](#oda)
      - [SDA](#sda)
      - [DDA](#dda)
      - [CDA](#cda)
    - [CVM](#cvm)
      - [Pasos de CVM](#pasos-de-cvm)
      - [CVM Result](#cvm-result)
  - [Analisis de accion de terminal](#analisis-de-accion-de-terminal)
    - [Parametros de Riesgo del Terminal (ADQUIRENTE)](#parametros-de-riesgo-del-terminal-adquirente)
    - [TAC Terminal Action Codes](#tac-terminal-action-codes)
    - [IAC Issuer Action Codes](#iac-issuer-action-codes)
    - [Primer Paso](#primer-paso)
    - [Sesguno Paso](#sesguno-paso)
    - [Caso Especial](#caso-especial)
    - [SOLICITUD DE CRIPTOGRAMA A LA TARJETA](#solicitud-de-criptograma-a-la-tarjeta)
  - [First Generate AC](#first-generate-ac)
  - [Autenticacion Online](#autenticacion-online)
  - [Second Generate AC](#second-generate-ac)
  - [Scripts Online](#scripts-online)
  - [Advice EMV](#advice-emv)
  - [Contactless](#contactless)
    - [Kernel](#kernel)
    - [CVM Requiered Limit](#cvm-requiered-limit)
    - [CVM Transaction Limit](#cvm-transaction-limit)
    - [CDCVM - Consumer Device CVM](#cdcvm---consumer-device-cvm)
    - [PIN OFFLINE sin cifrado Contactless](#pin-offline-sin-cifrado-contactless)
  - [EMV Certification](#emv-certification)
    - [EMVCo](#emvco)

## ¿Que es EMV?

EMV es un estandar de pago que permite a los usuarios realizar pagos con tarjetas de credito o debito en terminales de punto de venta (TPV) o cajeros automaticos (ATM) de forma segura.

## ¿Que es un TPV?

Un TPV es un terminal de punto de venta, es un dispositivo que permite realizar pagos con tarjetas de credito o debito.

## AID Application Identifier

Un identificador de aplicación (AID) se utiliza para identificar una aplicación en la tarjeta. 

Un AID consiste en un registro de proveedores de aplicaciones (**RID** - Register Identifier) de cinco bytes. Eg:

- A0 00 00 00 03  **VISA**
- A0 00 00 00 04  **MasterCard**
- A0 00 00 00 25  **AMEX**

Esto es seguido por una extensión propietaria identificador de aplicación (**PIX** - Proprietary Application Identifier) que permite que el proveedor de la aplicación pueda diferenciar entre las diferentes aplicaciones que se ofrecen. El AID debe imprimirse en todos los recibos EMV.

### Seleccion de Aplicaciones

Cuando se inserta una tarjeta, el terminal lee las aplicaciones (AIDs) que contiene y arma una lista. Esta lista es comparada con la lista de AIDs configurada en el terminal.

#### Comparaciones de AID

- Exacta: Los AID deben coincidir exactamente
- Parcial: Los AID de la tarjeta puede contener caracteres extra, solo debe coincidir la primera parte.

**Resultados:**

- No hay concidencia. La transaccion termina ahi. Se puede hacer fallback a la banda magnetica
- Una sola coincidencia. La aplicacion es seleccionada automaticamente
- Mas de una coincidencia. El terminal arma una lista y el usuario selecciona la aplicacion

#### Selección por PSE

![](Images/PSE.png)

```
Seleccionar el PSE
Leer el fichero DIR
El fichero DIR contiene:
    Una lista de entradas (DDF o ADF)
    Información acerca de la aplicación (Nivel de prioridad, Opción del titular,..)
```

#### SELECCIÓN FINAL

La aplicación puede estar bloqueada (PSE).

En la respuesta la tarjeta puede solicitar un PDOL (Processing Options Data Object List). Puede leerse como “condiciones de uso”

#### Selección por lista de AID’s

![](Images/LIstaAID.png)

El terminal puede decidir seleccionar las aplicaciones con su propia lista de AID

- Si la aplicación no es conocida por la tarjeta, no es agregada en la lista de candidatos y elterminal pasa al siguiente AID
- Si la aplicación es reconocida por la tarjeta, se agrega entonces en la lista de candidatos yel terminal pasa al siguiente AID
- FCI (Fichero Control de Información) nos retorna la Prioridad de la Aplicación (Nivel deprioridad, Opción del titular,..)

## ¿Que es un Data Object?

Un data object es un campo de datos que se utiliza para almacenar informacion en una tarjeta inteligente. Los data objects se codifican en TLV.

### Data Object List

Un data object list es una lista de data objects que se envian al terminal de punto de venta. Son una lista de las etiquetas y las longitudes de los elementos de datos.

### TLV - Tag Length Value

Los Data object se codifican en TLV. El primer byte es el tag, el segundo byte es el length y el resto es el value.

#### Tag

El tag es un identificador unico de un data object. El tag se compone de un byte o dos bytes. El primer bit del primer byte indica si el tag es de un byte o dos bytes. Si el primer bit es 0, el tag es de un byte. Si el primer bit es 1, el tag es de dos bytes. El resto de los bits del primer byte son el primer byte del tag. El segundo byte es el segundo byte del tag.

| b8 | b7 | b6 | b5 | b4 | b3 | b2 | b1 | explanation                       |
|----|----|----|----|----|----|----|----|-----------------------------------|
| 0  | 0  |    |    |    |    |    |    | universal class                   |
| 0  | 1  |    |    |    |    |    |    | application class                 |
| 1  | 0  |    |    |    |    |    |    | context specific class            |
| 1  | 1  |    |    |    |    |    |    | private class                     |
|    |    | 0  |    |    |    |    |    | primitiv data object              |
|    |    | 1  |    |    |    |    |    | constructed data object           |
|    |    |    | x  | x  | x  | x  | x  | tag value                         |
|    |    |    | 1  | 1  | 1  | 1  | 1  | there is a 2nd byte with tag value|

**Optional Byte 2:**

| b8 | b7 | b6 | b5 | b4 | b3 | b2 | b1 | explanation                   |
|----|----|----|----|----|----|----|----|-------------------------------|
| x  |    |    |    |    |    |    |    | b8 = 0 : this is the last byte|
|    | x  | x  | x  | x  | x  | x  | x  | tag value                     |

#### Length

El length es el numero de bytes que ocupa el value.

|No of Bytes| Length    | Coding                     |
|:---------:|:---------:|:---------------------------|
|     1     |    0-127  | 0xxxxxx                    |
|     2     |  128-255  | 10000001 xxxxxxxx          |
|     3     | 256-65535 | 10000010 xxxxxxxx xxxxxxxx |

#### Value

El value es el valor del data object.

## Inicio de transaccion

EL terminal envía comando hacia la tarjeta indicando que inicia la transacción (incluye PDOL si la tarjeta lo solicitó)

![](Images/InicioTransaccionEMV.png)

La tarjeta puede analizar el contenido del PDOL y decidir que no puede realizar la transaccion, se debe usar otra aplicacion. Si no hay, se cancela.

- **AFL:** (Application File Locator) Lista de entradas referenciando archivos dentro de la tarjeta.
- **SFI:** (Short File Identificator) Identificacion del archivo, primer y ultimo registro a leer. Registros para utilizar durante la autenticación de la tarjeta.
  - De acuerdo al AFL, el terminal encuentra dentro de la tarjeta los datos que precisa para proceasr la transacción.
- **AIP:** (Application Interchange Profile) Registro que indica las capacidades de la tarjeta para realizar funciones especificas. 

## Verficacion

### TSI

Transaccion Status Information es un registro de 2 bytes que indica las verificaciones hechas en la transaccion de la transaccion.

 El primer byte es:

| b8 | b7 | b6 | b5 | b4 | b3 | b2 | b1 | explanation                       |
|----|----|----|----|----|----|----|----|-----------------------------------|
| 1  |    |    |    |    |    |    |    | offline data authentication performed|
|    | 1  |    |    |    |    |    |    | cardholder verification performed|
|    |    | 1  |    |    |    |    |    | card risk management performed|
|    |    |    | 1  |    |    |    |    | issuer authentication performed|
|    |    |    |    | 1  |    |    |    | terminal risk management performed|
|    |    |    |    |    | 1  |    |    | script processing performed|
|    |    |    |    |    |    | 1  |    | reserved |
|    |    |    |    |    |    |    | 1  | reserved |

El segundo byte esta reservado



### TVR

Terminal Verification Results (TVR) es un registro que contiene los resultados de las verificaciones realizadas por el terminal. El registro TVR es un registro de 5 bytes. Cada bit del registro TVR indica si una verificación fue exitosa o no.

- Si el bit es 0, la verificación fue exitosa. 
- Si el bit es 1, la verificación no fue exitosa.

### ODA

Offline Data Authentication (ODA) se pretende validar la autenticidad de la tarjeta empleando criptografía de clave pública. EMV contempla tres métodos diferentes dependiendo de las características del chip y las necesidades de seguridad:

- Static Data Authentication (SDA)
- Dynamic Data Authentication (DDA)
- Combined Data Authentication (CDA)

#### SDA

Static Data Authentication (SDA) es un método de autenticación que se utiliza cuando la tarjeta no tiene un chip con capacidad de autenticación. El terminal genera un número aleatorio y lo cifra con la clave pública de la tarjeta. La tarjeta descifra el número aleatorio y lo compara con el número aleatorio que generó el terminal. Si los números aleatorios son iguales, la autenticación es exitosa.

No es muy seguro, hasta hay un tag en el TVR que indica que se utilizo SDA.

#### DDA

Dynamic Data Authentication (DDA) es el método más seguro. Se utiliza cuando la tarjeta dispone de un chip con capacidad de generar claves de forma dinámica. El terminal genera un par de claves pública y privada. La clave pública se envía a la tarjeta y la clave privada se almacena en el terminal. La tarjeta genera una clave pública y la envía al terminal. El terminal genera un mensaje cifrado con la clave privada y la clave pública de la tarjeta. La tarjeta descifra el mensaje con su clave privada y la clave pública del terminal. Si el mensaje descifrado es correcto, la tarjeta envía un mensaje de confirmación al terminal. El terminal verifica que el mensaje de confirmación es correcto. Si el mensaje de confirmación es correcto, la tarjeta es auténtica.

Si llegaran a cambiar la tarjeta despues de la autenticacion DDA, el terminal no lo sabria y podria seguir usando la tarjeta no autentica.

#### CDA

Combined Data Authentication (CDA) es un método de autenticación que se utiliza cuando la tarjeta dispone de un chip con capacidad de generar claves de forma dinámica y la tarjeta no dispone de un chip con capacidad de autenticación. El terminal genera un par de claves pública y privada. La clave pública se envía a la tarjeta y la clave privada se almacena en el terminal. La tarjeta genera una clave pública y la envía al terminal. El terminal genera un mensaje cifrado con la clave privada y la clave pública de la tarjeta. La tarjeta descifra el mensaje con su clave privada y la clave pública del terminal. Si el mensaje descifrado es correcto, la tarjeta envía un mensaje de confirmación al terminal. El terminal verifica que el mensaje de confirmación es correcto. Si el mensaje de confirmación es correcto, la tarjeta es auténtica.

Es el metodo mas seguro.

### CVM

Cardholder verification method

La Verificación del titular de la tarjeta se utiliza para evaluar si la persona que presenta la tarjeta es el titular legítimo. Los métodos de verificación del titular de la tarjeta son:

- Firma
- Plaintext PIN offline
- PIN cifrado offline
- Plaintext PIN y firma offline
- PIN y firma cifrada offline
- PIN Online
- No se requiere CVM
- Procesamiento de Falla CVM.

El terminal usa una lista CVM enviada por la tarjeta para determinar el tipo de verificación a realizar. La lista CVM establece una prioridad de CVMS para ser utilizados en relación con las capacidades del terminal. Diferentes terminales soportan diferentes CVMS.

#### Pasos de CVM

Las reglas de proceso están en orden secuencial (Lista de CVM)

- Verificar condiciones
- Ejecutar código asociado a la regla actual
- Chequear resultado de la ejecución anterior
- Si la regla se ejecutó con éxito: Verificación de titular terminada.
- Si la regla no es válida o falló la ejecución:
  - Pasar al CVR siguiente, si aparecen las condiciones CVR “Apply next CVR”
  - Si no, salir poniendo el bit “verificación titular tarjeta falló” en TVR

#### CVM Result

El resultado de la verificación del titular de la tarjeta se almacena en el registro CVM Result (CVR). El registro CVM Result es un registro de 2 bytes. Cada bit del registro CVM Result indica si una verificación fue exitosa o no.

## Analisis de accion de terminal

### Parametros de Riesgo del Terminal (ADQUIRENTE)

- **Lista negra.** Si el terminal maneja listas de tarjetas prohibidas, puede setear el bit del TVR “Card Appears on Terminal Exception File”.
- **Forzar Online.** Si el terminal tiene capacidad Online. Puede forzar la transacción ONLINE, seteando el bit del TVR “Merchant Forced Transaction Online”.
- **Velocity checking.** Utiliza los siguientes datos de la tarjeta para setear el bit del TVR “Consecutive Offline Limit Exceeded” o “New Card” si ATC es 0
    - **ATC** – Contador secuencial de transacciones.
    - **Last Online ATC** – Ultima transaccion ONLINE.
    - Máxima cantidad de transacciones offlines consecutivas.
- **Piso Limite.** Monto a partir del cual las transacciones deben ser autorizadas ONLINE.
- **Probabilidad mínima.** Para transacciones con montos por debajo del “Piso límite” está es la probabilidad requerir autorizaciones ONLINE
- **Umbral.** Monto inferior al “Piso límite” a partir del cual la probabilidad de requerir autorización ONLINE crece de forma lineal.

![](Images/ProbabilidadRiesgoTerminal.png)

### TAC Terminal Action Codes

Estructura identica al TVR y hay 3 tipos de TAC:

- DENEGAR (Denial) – Para decidir si debe denegar offline
- ONLINE – Para decidir si debe requerir autorización ONLINE
- DEFAULT – Utilizado en casos especiales

### IAC Issuer Action Codes 

Seteado en la terjeta por el emisor. Estructura identica al TVR y hay 3 tipos de IAC:

- DENEGAR (Denial) – Para decidir si debe denegar offline
- ONLINE – Para decidir si debe requerir autorización ONLINE
- DEFAULT – Utilizado en casos especiales

### Primer Paso

DENEGAR (Denial) – Para decidir si debe denegar offline

Se procesa bit a bit con el algoritmo: TVR AND (TAC OR IAC)

- Si algún bit es “1” el terminal decide denegar OFFLINE la transacción, es decir informará a la tarjeta que toma la decisión de denegar OFFLINE.
- Si ningún bit es “1” el terminal evalua el siguiente TAC/IAC.

### Sesguno Paso

ONLINE – Para decidir si debe requerir autorización ONLINE

Se procesa bit a bit con el algoritmo: TVR AND (TAC OR IAC)

- Si algún bit es “1” el terminal decide requerir autorización ONLINE, es decir informará a la tarjeta que toma la decisión de requerir autorización ONLINE.
- Si ningún bit es “1” el terminal decide aprobar la transacción OFFLINE, es decir informará a la tarjeta que toma la decisión de aprobar OFFLINE.

### Caso Especial

DEFAULT – En el caso de terminales sin capacidad ONLINE

Se procesa bit a bit con el algoritmo: TVR AND (TAC OR IAC)

- Si algún bit es “1” el terminal decide requerir autorización ONLINE, es decir informará a la tarjeta que toma la decisión de requerir autorización ONLINE.
- Si ningún bit es “1” el terminal decide aprobar la transacción OFFLINE, es decir informará a la tarjeta que toma la decisión de aprobar OFFLINE.

### SOLICITUD DE CRIPTOGRAMA A LA TARJETA

- Si el terminal decide denegar OFFLINE, solicita a la tarjeta un criptograma tipo AAC (Application Authentication Cryptogram)
- Si el terminal decide requerir autorización ONLINE, solicita a la tarjeta un criptograma tipo ARQC (Authorization Request Cryptogram)
- Si el terminal decide aprobar OFFLINE, solicita a la tarjeta un criptograma tipo TC (Transaction Certificate)

![](Images/RisgoTarjetaPOS.png)

## First Generate AC

El CDOL1 (Lista de objetos de datos de la tarjeta) es una lista de etiquetas que la tarjeta quiere enviar para tomar una decisión sobre si se debe aprobar o rechazar la transacción (incluyendo monto de la transacción, pero muchos otros objetos de datos también). El terminal envía estos datos y solicita un criptograma con el comando generar criptograma de aplicación. En función de la decisión de la terminal (fuera de línea, en línea, el declive), la terminal solicita una de las siguientes criptogramas de la tarjeta :

- Certificado de transacción (TC) - Desconectado aprobación
- Autorización de Solicitud de Autorización del criptograma (ARQC) -Online
- La aplicación de autenticación criptograma (AAC) Desconectado declive.

Este paso de la tarjeta da la oportunidad de aceptar el análisis de la actuación de la terminal o rechazar una transacción o forzar una transacción en línea. La tarjeta no puede devolver un TC cuando un ARQC se ha solicitado, pero puede devolver un ARQC cuando un TC se ha solicitado

## Autenticacion Online

## Second Generate AC

De acuerdo al código de respuesta del host y al resultado de la autenticación del emisor la tarjeta entrega al terminal el resultado final

Si en este punto la tarjeta deniega una transacción aprobada por el host, el terminal deberá reversar.

## Scripts Online


## Advice EMV

El advice emv es una transaccion que se realiza cuando el POS no puede comunicarse con el host. Se guarda en el campo 55 del mensaje ISO 8583 los criptogramas y tags. El advice emv se envia al host cuando se vuelve a conectar el POS.

## Contactless

Funcionan mediante nfc (near field communication) y se comunican con el POS mediante un chip. El POS debe tener un lector de tarjetas contactless.

### Kernel

El kernel se identifica en el campo 9F2A e indica que tipo de kernel soporta la tarjeta:


Si no se encuentra el identifier se utiliza:

- JCB ADF name usa kernel 5
- MasterCard ADF name usa kernel 2
- Visa ADF name usa kernel 3
- AMEX ADF name usa kernel 4

### CVM Requiered Limit

El CVM Requiered Limit es un valor que indica el monto máximo para el cual se puede realizar una transacción contactless sin requerir una verificación del titular de la tarjeta. Es para agilizar el proceso de pago.

### CVM Transaction Limit

El CVM Transaction Limit es un valor que indica el monto máximo para el cual se puede realizar una transacción contactless. 

### CDCVM - Consumer Device CVM

Este metodo se usa cuando se paga con un celular, el POS va a pedir al dispositivo la validacion del cardholder. La validacion va a depender de la aplicacion que se haya utilizado para pagar.

### PIN OFFLINE sin cifrado Contactless

No se utiliza mas en contactless por se considerado poco seguro.

## EMV Certification

### EMVCo

EMVCo es una organización que se encarga de certificar los terminales POS y los dispositivos de pago.

