# ONBOARDING ELDAR

- [ISO9001](ISO9001.md)
- [Metodos de Pago](MetodosDePago.md)
- [ISO8583](ISO8583.md)
- [Criptografia](Criptografia.md)
- [EMV](EMV.md)
- [Transacciones Financieras](TransaccionesFinancieras.md)