# Metodos de Pagos

# Que es un POS?

**POS:** Point of Sale

Es una terminal seguro donde se completa una transaccion electronica. Acepta el ingreso de tarjetas por banda, manual, QR y tarjetas inteligentes (EMV)

Cuenta con teclado seguro para el ingreso de pin y una impresora termica

Puede ser:

- **Autoservicio:** Terminales desatendidos, intuitivos y faciles de usar. Eg: Maquina expendedora
- **Fijos:** No tienen bateria y no son faciles de transportar. Suelen sr de dial up
- **Portatiles:** Con bateria, Compatibles con GPRS, WIFI o Bluetooth.

## Que es un adquirente?

Entidad financiera con licencia Visa, Mastercard, Amex que suministra  las terminales al comercio o vendedor para aceptar tarjetas de crédito  o débito.

Eg: Prisma o Fiserv

## Que es una entidad Emisora?

Es el banco, entidad de crédito, ahorro, prestamos o entidad gubernativa o negocio minorista que ofrece una linea de credito o una tarjeta de debito a un consumidor.

Eg: BBVA, Santander, etc...

## Que es una red de tarjeta?

Proporciona la tecnología y la red para hacer posibles las transacciones.

Eg: Amex, Visa y Mastercard

## Proceso de Pago

![](Images/ProcesoDePago.png)

## Tecnologias de Tarjetas

La prioridad de uso es:

1. Sin contacto
2. Contacto
3. Banda

### Banda - Magstripe

Almacena datos modificando el magnetismo de pequeñas particulas magneticas

### CHIP contacto - EMV

Contiene un circuito integrado que es alimentado al ser insertado en un lector. Es capaz de almacenar informacion y procesar datos

### CHIP sin contacto - EMV

Se comunican con el lector de tarjetas a traves de radiofrecuencias

## Datos de las Tarjetas - PAN

### Que es el PAN?

- Primary Acount Number
- Su longitud puede ser 14 a 19 digitos

![](Images/PAN.png)

### Que es el MII?

Identifica el tipo de sistema al que la tarjeta está asociada.

- 0: ISO/TC 68 y otros
- 1: Aerolíneas
- 2: Aerolíneas y otros
- 3: Viajes, entretenimiento y finanzas (American  Express, JCB y Diners Club)
- 4: Banca y finanzas (VISA)
- 5: Banca y finanzas (MasterCard)
- 6: Mercadeo y banca/finanzas (Discover)
- 7: Empresas petroleras y otros
- 8: Salud, telecomunicaciones y otros
- 9: Asignaciones futuras

### Que representa el BIN?

Identifica el banco emisor de la tarjeta

### Que representa el IAI?

Identifica el numero de cuenta asociado al titular.

### CHeck - Algoritmo de Luhn?

Tambien se conoce por "algoritmo de modulo 10". Es una fromula de suma de verificacion 

#### Como se obtiene?

1. Tomando el PAN, se duplican de derecha a izquierda los numeros en posiciones pares
2. Los resultados duplicados se suman digito a digito. Eg: 16 -> 1+6=7
3. Se suman todos los digitos
4. Se le hace modulo 10 al numero y se obtiene el digito de chequeo

## POS y Pinpads

- Similitudes
  - Lector de tarjeta
  - Teclado para ingreso de PIN
- Diferencias
  - Impresoras (Solo POS)
  - Conectividad
  - Autonomia (Solo POS)

### Como funciona POS

1. Ingresa monto y cuotas en el POS
2. Apoya, inserta o pasa la tarjeta al POS
3. POS se comunica con el host
4. POS informa la respuesta e imprime el ticket

### Como funciona el PINpad

1. Ingresa monto y cuotas en el sistema propio
2. Sistema propio envia comando de lectura de tarjeta al PINpad
3. Apoya, inserta o pasa la tarjeta en el PINpad
4. Sistema propio arma el mensaje y se comunica con el host
5. Sistema propio informa la respuesta e imprime el ticket