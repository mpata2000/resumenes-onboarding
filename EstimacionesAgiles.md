# Estimaciones

## Estimando un proyecto completo

### Entendimiento comun

Primer paso para estimar el proyeto completo es tener una vision compartida con todos los involucrados

Hay que poner foco en las persona y el producto, para hacer eso se crea un evento acotado para trabajar en tener una vision completa del problema a resolver, sus limites y el alcance. 

En esta reunion hay que incluir a gente de distintas areas. El foco es tener las discusiones internas importantes del producto para lograr una vision compartida.

### Participantes

Los participantes de este evento tienen que tener diversidad (distintas areas de la organizacion, distintos roles y posiciones de jerarquia). Pero tambien importa la relevancia que tienen en el proyecto (entendimiento de la plataforma y los procesos).

En la convocatoria hay que explicar porque se los convoca y por cuanto tiempo para incentivar la participacion.

Cuando se empieza el el evento se generan grupos interdiciplinarios para dividir y reportar y generar mayor participacion.

### Actividades

- **Impact Mapping**: Una tecnica donde se hace un mapa mental donde el nodo principal es el objetivo/problema que vamos a resolver. De ahi se sacan distintas ramas de los actores involucrados en la resolucion del problema, de cada actor tambien se sacan ramas con el impacto que tendra el producto. De los impactos se generan los entregables que hay que tener en cuenta.
- **Incepcion Agil**: SOn 10 actividades, siendo la primera la inttroduccion hablar del rol de cada participantes. EL resto de actividades son dividir y reportar
  - Elevetor Pitch
  - Vision Box
  - Que si, que no
  - La comunidad
  - La solucion
  - Los riesgos
  - Tamaño
  - Trade-off
  - Costos Involucrados
- **The Vision Board** es un estilo de canvas donde vemos el objetivo de negocio, grupos de interes y objetivos del producto
- **Story Mapping**

### Facilitacion

El facilitador va tener que v¿facilitar que suceda el evento y que se logre una vision comun

1. Convocatoria: Definir la convocatoria y enviarla (Mail o charlado)
2. Preparacion previa: Encontrar el espacio adecuado con comida y bebida.
3. El evento: Organizar los grupos pequeños y mas diversos posibles y facilitar las actividades.
4. Actividades: Presentar la consigna claramente, facilitar el trabajo teniendo todos los materiales y ayudando a los grupos. Despues ayudar a comparar los resultados de cada equipo
5. Discusion: Discutir los resultados y tratar de converger las distintas ideas
6. Presentar los resultados

Recomendaciones:

- Actividades con cosas manuales y un toque mas de movimiento.
- Mantener el foco

### Entregas Frecuentes y valiosas

En las metodologias agiles tenemos entregas frecuentes y valiosas para evitar la acumulacion de riesgo. En cada sprint vamos a tener un producto que funciona para ver si cumple con nuestras expectativas hasta llegar un producto que podemos empezar a utilizar.

### Afinando entregas

Para afinar las entregas se pueden utilizar:

- Story mapping: **TODO**
- Casos de Uso
- Emepzar a con un segmento especifico del producto para una audencia especifica

## Estimando cada Sprint

### Estimaciones relativas

**TODO**


