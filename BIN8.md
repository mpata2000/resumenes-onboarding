# BIN8

- Debido a los cambios en el mercado actual, ya sea por nuevos productos, nuevos jugadores u otros factores, la demanda de BIN´s a aumentado fuertemente. Para afrontar dicha demanda, las banderas (MasterCard / Visa / etc.) han publicado un mandato para redefinir el esquema actual de uso del BIN (Bank Identification Number).
- En Abril del 2022 entrará en vigencia la obligación de expandir la cantidad de dígitos del BIN pasando de 6 a 8 dígitos. El cambio es mandatorio únicamente para los adquirentes, siendo por ahora una recomendada para los emisores. Entendemos que será cuestión de tiempo para que el cambio sea mandatorio para todos los jugadores.

## A Tener en cuenta

![](Images/PAN.png)

- Si bien los dígitos para identificar un BIN pasarán de 6 a 8, los dígitos que conforman el PAN no sufrirán modificación.
- Los primeros 6 dígitos del PAN se los conoce como BIN (Bank Identificación Number) o IIN (Issuer Identification Number)
-  El primer digito del BIN es el MII (Major Industry Identifier) que es el identificador principal de la industria.
- Esta estructura numérica permite que a cada emisor de tarjetas se le asigne un rango de dígitos que le permitirán identificar a las tarjetas que hayan sido emitidas bajo su responsabilidad. Una de las principales problemáticas es que esos rangos se están agotando y de allí la necesidad de ampliar de 6 a 8 dígitos.
- Los 2 dígitos que gana el BIN los pierde el Nro de cuenta individual.
- En el cambio se refuerza el hecho de que el rango de bines debe ser usado correctamente. Consiste en utilizar los siguientes dígitos después del BIN para segmentar la familia de productos del emisor (gold platino black)
-  El BIN es la estructura numérica que permite a cada emisor asignar un rango de dígitos que le permita identificar las tarjetas que emita bajo su responsabilidad.
- Con el cambio de 6 a 8 dígitos para el bloque identificador del BIN pasará de poder emitir 1000 millones de tarjetas de un único BIN a 10 millones.

## Implementacion de BIN8

![](Images/ImplementacionBIN8.png)

### BIN

- Los primeros 6 digitos del PAN(estandar actual)
- Identificador del Emisor
- Integral para la distribucion de la transaccion (Autorizacion)
- El Nuevo BIN Estandar de la ISO estara compuesto por los primeros 8 digitos del PAN

### Funciones del BIN

- Identificar de manera unica las redes,emisoress y otras informacion importante.
- SOn un elemento de los pagos electronicos y se deben manejar de forma eficaz
- Es necesario un suministro continuo para ayudar a garantizar un ecosistema de pagos global, seguro y sostenible.

## Rangos de Cuenta

- Los rangos de cuentas estan compuestos de hasta los primeros 11 digitos de un PAN.
- Maximisa la eficiencia del BIN.
- Se puede usar para segmentar aun mas una cartera de producto y geografia.
- Define reglas para el intercambio y los beneficios relacionados con el producto.

## Presentacion

![](Images/BIN8-1.jpg)
![](Images/BIN8-2.jpg)
![](Images/BIN8-3.jpg)
![](Images/BIN8-4.jpg)
![](Images/BIN8-5.jpg)